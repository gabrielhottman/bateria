package com.example.bateria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Bateria extends AppCompatActivity {

    //1er Paso
    private BroadcastReceiver mReceiver;
    private TextView txtTexto;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bateria);
        txtTexto = findViewById(R.id.textView);
        progressBar = findViewById(R.id.progressBar);

        mReceiver = new BatteryBroadcastReceiver();
        registerReceiver(mReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }


    @Override
    protected void onStart(){
        super.onStart();
        registerReceiver(mReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    //Paso: Crear una clase privata interna  BatteryBroadcastReceiver
    private class BatteryBroadcastReceiver extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL,0);
            txtTexto.setText(level + " "+ getString(R.string.nivelBateria));
            progressBar.setProgress(level);
        }
    }
}
