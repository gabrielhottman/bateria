package com.example.bateria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.bateria.ui.login.LoginActivity;

public class Splash extends AppCompatActivity {

    //Constante de tipo entero que representa el tiempo en
    //millisegundos que se muestra actividad al usuario
    static int TIMEOUT_MILLIS = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Asignamos el archivo xml que va a ligarse como diseño de la pantalla
        setContentView(R.layout.activity_splash);

        //Quitamos el action bar de la pantalla Splash
        getSupportActionBar().hide();
        //La clase Handle permite enviar objetos de un tipo Runnable.
        //Un objeto de tipo unnable dá el significado a un hilo de proceso


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Intent es la definicion abstarcta de una operación a realizar
                //Puede ser usado parauna activity, broadcast receiver, servicios.
                Intent i = new Intent(Splash.this, LoginActivity.class);
                startActivity(i);
                //Finaliza esta activity
                finish();
            }
        }, TIMEOUT_MILLIS);


    }
}
