package com.example.bateria.ui.login;

/**
 * Clase que expone detalles de usuarios autenticados a la IU.
 */
class LoggedInUserView {
    private String displayName;
    //... otros campos de datos que pueden ser accesibles para la interfaz de usuario

    LoggedInUserView(String displayName) {
        this.displayName = displayName;
    }

    String getDisplayName() {
        return displayName;
    }
}
